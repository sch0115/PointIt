import Vue from 'vue';
import './index.scss';
import router from './app/app.router';
import store from './app/app.store';
import VueFire from 'vuefire';
import {MediaQueries} from 'vue-media-queries';

import MainView from './app/pages/Main.vue';

Vue.use(VueFire);

export default new Vue({
  el: '#root',
  router,
  store,
  MediaQueries,
  render: h => h(MainView)
});
