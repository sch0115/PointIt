import Vue from 'vue';
import Vuex from 'vuex';

import {app} from './__vuex/app.module';
import {game} from './__vuex/game.module';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,

  modules: {
    app,
    game
  }
});

export default store;
