import Firebase from 'firebase';
import {APP_SETTINGS} from '../app.settings';

const firebaseApp = Firebase.initializeApp(APP_SETTINGS.FIREBASE_CONFIG);

export const db = firebaseApp.database();
