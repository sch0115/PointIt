// import Store from '../app.store';

const mqtt = require('mqtt');
const client = mqtt.connect('ws://broker.hivemq.com:8000');

client.on('connect', () => {
  client.subscribe('presence');
  client.publish('presence', 'Hello mqtt');
});

client.on('message', (topic, message) => {
  // message is Buffer
  console.log(message.toString());
  client.end();
});

module.export = {
  publish: (topic, message) => {
    client.publish(topic, message);
  },
  subscribe: topic => {
    client.subscribe(topic);
  }
};
