const MODULE_NAME = 'UtilityService';

export default {
  name: MODULE_NAME,

  /**
   * Global logging.
   * @param module {string}
   * @param message {string|object}
   */
  logging(module, message) {
    const devMode = process.env.NODE_ENV === 'development';

    if (devMode && message) {
      if (typeof message === 'object') {
        console.log(module);
        console.log('==========');
        console.log(message);
        console.log('==========');
      } else {
        console.log(module + ' : ' + message);
      }
    }
  }
};
