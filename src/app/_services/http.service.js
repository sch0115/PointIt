import Vue from 'vue';
import VueResource from 'vue-resource';

import UtilityService from './utility.service';

import {APP_SETTINGS} from '../app.settings';

const MODULE_NAME = 'HttpService';

Vue.use(VueResource);

Vue.http.options.root = APP_SETTINGS.FIREBASE_CONFIG.databaseURL;

export default {
  name: MODULE_NAME,
  get(url, options = {}) {
    return Vue.http.get(url, options);
  },

  post(url, body, options = {}) {
    return Vue.http.post(url, body, options);
  },

  _log(message) {
    UtilityService.logging(MODULE_NAME, message);
  }
};
