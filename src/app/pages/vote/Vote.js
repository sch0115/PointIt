import UtilityService from '../../_services/utility.service';
import HttpService from '../../_services/http.service';
import {db} from '../../_services/firebase.service';
import {APP_SETTINGS} from '../../app.settings';

const MODULE_NAME = 'Vote';

const DEFAULT_VALUES = {
  user: {
    userName: 'User',
    vote: 0,
    avatarId: 0,
    active: true
  }
};

export default {
  name: MODULE_NAME,

  data() {
    return {
      roomId: null,
      userId: null,
      room: null,
      user: null,
      userName: this.$route.query.userName,
      roomNotFound: false,
      roomNumber: this.$route.query.roomNumber,
      points: 1,
      roomNumberField: '',
      roomNumbers: null,
      disableInput: false
    };
  },

  computed: {
    connected() {
      return Boolean(this.user);
    },
    ctaButtonText() {
      return 'Vote';
    }
  },
  created() {
    this._log('created.');
    if (this.$route.query.userName) {
      DEFAULT_VALUES.user.userName = this.$route.query.userName;
    }
    this.$watch('roomId', roomId => {
      if (roomId) {
        this.$bindAsObject('room', db.ref(`rooms/${roomId}`));
      }
    }, {
      immediate: true
    });
    this.$watch('roomNumberField', roomNumberField => {
      if (roomNumberField.length === 5) {
        this.disableInput = true;
        this.joinRoom(roomNumberField);
      }
    }, {
      immediate: true
    });
    this.$watch('userId', userId => {
      if (userId) {
        this.$bindAsObject('user', db.ref(`rooms/${this.roomId}/users/${userId}`));
      }
    }, {
      immediate: true
    });
    this.joinRoom(this.roomNumber);
  },
  mounted() {
    this._log('mounted.');
  },
  destroyed() {
    this._log('destroyed.');
  },
  methods: {
    joinRoom(roomNumber) {
      if (roomNumber) {
        HttpService.get(APP_SETTINGS.FIREBASE_CONFIG.databaseURL + '/roomNumbers.json')
          .then(response => {
            const roomNumbers = response.body;
            this.roomNumbers = roomNumbers;
            for (const key in roomNumbers) {
              if (Object.prototype.hasOwnProperty.call(roomNumbers, key)) {
                if (roomNumbers[key].roomNumber.toString() === roomNumber.toString()) {
                  this.roomId = roomNumbers[key].roomId;
                }
              }
            }
            this.createUser();
          });
      } else {
        this.roomNotFound = true;
      }
    },
    createUser() {
      console.log('createUser');
      const savedUser = localStorage.getItem('user');
      const newUser = {
        userName: DEFAULT_VALUES.user.userName + Math.round((Math.random() * 1000) + 1000),
        active: DEFAULT_VALUES.user.active,
        vote: DEFAULT_VALUES.user.vote,
        avatarId: DEFAULT_VALUES.user.avatarId,
        created: new Date()
      };
      if (this.userName) {
        newUser.userName = this.userName;
      }
      if (savedUser) {
        savedUser.vote = 0;
        savedUser.active = true;
      }

      const user = savedUser ? savedUser : newUser;
      const userId = db.ref('rooms').child(this.roomId).push('users').push().key;
      db.ref('rooms')
        .child(this.roomId)
        .child('users')
        .child(userId).set(user);
      this.userId = userId;
    },
    onCtaButtonClicked() {
      console.log('rrom: ', this.user);
      db.ref('rooms')
        .child(this.roomId)
        .child('users')
        .child(this.userId)
        .child('vote').set(this.points);
    },
    addPoints() {
      if (this.points < 40) {
        this.points++;
      }
    },
    subtractPoints() {
      if (this.points > 1) {
        this.points--;
      }
    },
    _log(message) {
      UtilityService.logging(MODULE_NAME, message);
    }
  }
};
