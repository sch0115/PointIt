// import {mapState} from 'vuex';
import UtilityService from '../../_services/utility.service';
import HttpService from '../../_services/http.service';
import {db} from '../../_services/firebase.service';
import {APP_SETTINGS} from '../../app.settings';
import QRCode from 'qrcode';


const MODULE_NAME = 'Dashboard';

const DEFAULT_VALUES = {
  room: {
    roomName: 'PointIt room',
    topic: 'Important topic',
    users: []
  },
  user: {
    userName: 'User',
    vote: 0,
    avatarId: 1,
    active: true
  }
};

export default {
  name: MODULE_NAME,

  data() {
    return {
      room: null,
      roomId: null,
      roomNumber: this.$route.query.roomNumber,
      roomName: null,
      topic: null,
      users: null,
      qrCode: null
    };
  },

  computed: {
    averagePoints() {
      let sum = 0;
      const users = this.users;
      for (const key in users) {
        if (Object.prototype.hasOwnProperty.call(users, key)) {
          sum += users[key].vote;
        }
      }
      const roundedAverage = Math.round(sum / Object.keys(users).length * 10) / 10;
      return this.allVoted ? roundedAverage : '?';
    },
    modePoints() {
      const votes = [];
      const users = this.users;
      let mode = 0;
      for (let key = 0; key < users.length; key++) {
        votes.push(users[key].vote);
      }
      mode = this.calculateMode(votes);
      return this.allVoted ? mode.toString() : '?';
    },
    allVoted() {
      const users = this.users;
      if (users && Object.keys(users).length > 0) {
        let allVoted = true;
        for (const key in users) {
          if (Object.prototype.hasOwnProperty.call(users, key)) {
            if (users[key].vote === 0) {
              allVoted = false;
            }
          }
        }
        return allVoted;
      }
      return false;
    },
    votingStatus() {
      let message = 'Waiting for users to connect...';
      const users = this.users;
      if (users) {
        if (this.allVoted) {
          message = 'Everyone have voted. Check results!';
        }
        const userNamesOfUsersWhoHasNotVoted = [];
        for (let key = 0; key < users.length; key++) {
          if (users[key].vote === 0) {
            userNamesOfUsersWhoHasNotVoted.push(users[key].userName);
          }
        }
        if (userNamesOfUsersWhoHasNotVoted.length === users.length) {
          if (users.length === 0) {
            message = 'Waiting for users to connect...';
          } else {
            message = 'You can start voting now.';
          }
        } else if (userNamesOfUsersWhoHasNotVoted.length > 4) {
          message = 'Waiting for ' + userNamesOfUsersWhoHasNotVoted.length + ' more people to vote.';
        } else {
          message = 'Waiting for ';
          for (let i = 0; i < userNamesOfUsersWhoHasNotVoted.length; i++) {
            if (i === userNamesOfUsersWhoHasNotVoted.length - 1 && userNamesOfUsersWhoHasNotVoted.length > 1) {
              message += ' and ' + userNamesOfUsersWhoHasNotVoted[i];
            } else if (i === userNamesOfUsersWhoHasNotVoted.length - 2 || userNamesOfUsersWhoHasNotVoted.length === 1) {
              message += userNamesOfUsersWhoHasNotVoted[i];
            } else {
              message += userNamesOfUsersWhoHasNotVoted[i] + ', ';
            }
          }
          message += ' to vote.';
        }
      }
      return message;
    }
  },

  created() {
    this._log('created.');
    if (this.$route.query.roomName) {
      this.roomName = this.$route.query.roomName;
    }
    if (this.$route.query.topic) {
      this.topic = this.$route.query.topic;
    }
    this.$watch('roomId', roomId => {
      if (roomId) {
        this.$bindAsObject('room', db.ref(`rooms/${roomId}`));
        this.$bindAsArray('users', db.ref(`rooms/${roomId}/users`));
      }
    }, {
      immediate: true
    });
    this.$watch('users', users => {
      if (users) {
        for (let i = 0; i < users.length; i++) {
          if (user[i] === 10) {
            // create player instance
            constole.log('sjouldPlay');
          }
        }
      }
    }, {
      immediate: true
    });
    HttpService.get(APP_SETTINGS.FIREBASE_CONFIG.databaseURL + '/roomNumbers.json')
      .then(response => {
        const roomNumbers = response.body;
        if (this.roomNumber) {
          for (const key in roomNumbers) {
            if (Object.prototype.hasOwnProperty.call(roomNumbers, key)) {
              if (roomNumbers[key].roomNumber.toString() === this.roomNumber.toString()) {
                this.roomId = roomNumbers[key].roomId;
                return;
              }
            }
          }
        }
        console.log('should not be there if roomNumber present!');
        this.createRoom(this.generateRoomNumber(roomNumbers));
      });
  },
  updated() {
    this.$nextTick(() => {
      if (!this.qrcode) {
        this.generateQrCode();
      }
    });
  },
  mounted() {
    this._log('mounted.');
    // this.generateQrCode();
  },
  destroyed() {
    this._log('destroyed.');
  },
  methods: {
    removeUser(userId) {
      db.ref('rooms')
        .child(this.roomId)
        .child('users')
        .child(userId).remove();
    },
    restartVoting() {
      const users = this.users;
      for (let i = 0; i < this.users.length; i++) {
        db.ref('rooms')
          .child(this.roomId)
          .child('users')
          .child(users[i]['.key'])
          .child('vote').set(0);
      }
    },
    createRoom(roomNumber) {
      const newRoom = {
        roomNumber,
        roomName: this.roomName ? this.roomName : DEFAULT_VALUES.room.roomName,
        topic: this.topic ? this.topic : DEFAULT_VALUES.room.topic,
        users: []
      };
      console.log('room pushed');
      const roomId = db.ref('rooms').push(newRoom).key;
      this.roomId = roomId;
      db.ref('roomNumbers').push({roomNumber, roomId});
    },
    generateRoomNumber(roomNumbers) {
      let roomNumber = Math.floor(Math.random() * 90000) + 10000;

      while (!this.validateRoomNumber(roomNumbers, roomNumber)) {
        roomNumber = Math.floor(Math.random() * 90000) + 10000;
      }

      return roomNumber;
    },
    validateRoomNumber(roomNumbers, roomNumber) {
      let isValid = true;

      for (let i = 0; i < roomNumbers.length; i++) {
        if (roomNumbers[i].roomNumber === roomNumber) {
          isValid = false;
        }
      }
      return isValid;
    },
    generateQrCode() {
      const canvas = this.$refs.qrcode;
      if (canvas) {
        const qrCodeContent = 'pointit-be9d3.firebaseapp.com/Vote?roomNumber=' + this.room.roomNumber;
        QRCode.toCanvas(canvas, qrCodeContent, err => {
          if (err) {
            console.error(err);
            this._log(err);
          }
          this.qrCode = qrCodeContent;
          this._log('QR code generated!');
          this._log('QR code redirects to: ' + qrCodeContent);
        });
      }
    },
    calculateMode(array) {
      if (array.length < 2) {
        return array[0];
      }
      const hashTable = {};
      let mostFrequentCount = 0;
      const mode = [];
      for (let i = 0; i < array.length; i++) {
        if (hashTable[array[i]]) {
          hashTable[array[i]]++;
          if (hashTable[array[i]] > mostFrequentCount) {
            mostFrequentCount = hashTable[array[i]];
          }
        } else {
          hashTable[array[i]] = 1;
        }
      }
      for (const key in hashTable) {
        if (Object.prototype.hasOwnProperty.call(hashTable, key)) {
          if (hashTable[key] === mostFrequentCount) {
            mode.push(key);
          }
        }
      }
      return mode;
    },
    _log(message) {
      UtilityService.logging(MODULE_NAME, message);
    }
  }
};
