export const app = {
  namespaced: true,

  state: {
    stateDisplayModal: false,
    stateModalComponent: null,
    stateModalExternalWeb: null,
    navBarSearch: ''
  },

  getters: {},

  mutations: {
    SET_DISPLAY_MODAL(state, displayModal) {
      state.stateDisplayModal = displayModal;
    },
    SET_MODAL_COMPONENT(state, modalComponent) {
      state.stateModalComponent = modalComponent;
    },
    SET_MODAL_EXTERNAL_WEB(state, externalUrl) {
      state.stateModalExternalWeb = externalUrl;
    },
    SET_NAV_BAR_SEARCH(state, navBarSearch) {
      state.navBarSearch = navBarSearch;
    }
  },

  actions: {
    setDisplayModal({commit}, displayModal) {
      if (!displayModal) {
        commit('SET_MODAL_COMPONENT', null);
      }
      commit('SET_DISPLAY_MODAL', displayModal);
    },
    setModalComponent({commit}, modalComponent) {
      commit('SET_MODAL_COMPONENT', modalComponent);
    },
    setModalExternalWeb({commit}, externalUrl) {
      commit('SET_MODAL_EXTERNAL_WEB', externalUrl);
    },
    setNavBarSearch({commit}, navBarSearch) {
      commit('SET_NAV_BAR_SEARCH', navBarSearch);
    }
  }
};
