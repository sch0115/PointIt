export const stories = {
  namespaced: true,

  state: {
    stories    : [{
      id: 1,
      title: 'Story title',
      startDate: null,
      endDate: null,
      continent: '',
      description: '',
      public: false
    }],
    currentStory  : null,
  },

  getters: {
    getStories() {
      return this.stories;
    },
    getAllStories() {
      return this.stories;
    },
    getStoryById(storyId) {
      return state.stories.find((story) => story.id === storyId);
    }
  },

  mutations: {
    SET_STORIES(state, stories) {
      state.stories = stories;
    },
    CREATE_STORY(state, story) {
      state.stories.push(story);
    },
    UPDATE_STORY(state, editedStory) {
      let storyToUpdate = this.getStoryById(editedStory.id);
      if (storyToUpdate){
          storyToUpdate = editedStory;
      } else {
        throw ReferenceError.message(`Story to update with id ${editedStory.id} does not exist`)
      }
    },
    DELETE_STORY(state, storyId) {
      // Will array 'stories' rearange and not corrupt?
        let storyToDelete = this.getStoryById(storyId);
        if (storyToDelete){
            storyToDelete.delete();
        } else {
            throw ReferenceError.message(`Story to delete with id ${storyId} does not exist`);
        }
    }
  },

  actions: {
    setStories({commit}, stories) {
      commit('SET_STORIES', stories);
    },
    createStory({commit}, story) {
      commit('CREATE_STORY', story);
    },
    updateStory({commit}, story) {
        commit('UPDATE_STORY', story);
    },
    deleteStory({commit}, story) {
        commit('DELETE_STORY', story);
    }
  }
};
