export const game = {
  namespaced: true,

  state: {
    room: null,
    userName: false,
    roomId: null,
    roomName: null,
    isHost: false
  },

  getters: {},

  mutations: {
    SET_ROOM(state, room) {
      state.room = room;
    },
    SET_USER_NAME(state, userName) {
      state.userName = userName;
    },
    SET_ROOM_NAME(state, roomName) {
      state.roomName = roomName;
    },
    SET_ROOM_ID(state, roomId) {
      state.roomId = roomId;
    },
    SET_IS_HOST(state, isHost) {
      state.isHost = isHost;
    }
  },

  actions: {
    setUserName({commit}, userName) {
      commit('SET_USER_NAME', userName);
    },
    setRoomName({commit}, roomName) {
      commit('SET_ROOM_NAME', roomName);
    },
    setRoomId({commit}, roomId) {
      commit('SET_ROOM_ID', roomId);
    },
    setRoom({commit}, room) {
      commit('SET_ROOM', room);
    },
    setIsHost({commit}, isHost) {
      commit('SET_IS_HOST', isHost);
    }
  }
};
