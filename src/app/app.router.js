import Vue from 'vue';
import VueRouter from 'vue-router';

import LandingPage from './pages/landingPage/LandingPage.vue';
import Dashboard from './pages/dashboard/Dashboard.vue';
import Vote from './pages/vote/Vote.vue';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      components: {
        default: LandingPage
      }
    },
    {
      path: '/Dashboard',
      component: Dashboard
    },
    {
      path: '/Vote',
      component: Vote
    }
  ]
});

export default router;
