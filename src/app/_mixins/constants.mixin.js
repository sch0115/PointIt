import {APP_SETTINGS} from '../app.settings';

const MODULE_NAME = 'ConstantsMixin';

export const constantsMixin = {
  name: MODULE_NAME,
  data() {
    return {
      oxygenEnum   : APP_SETTINGS.POC_LIST_VIEW_MODES.OXYGEN,
      batteriesEnum: APP_SETTINGS.POC_LIST_VIEW_MODES.BATTERIES,

      assignPocCreateEnum: APP_SETTINGS.ASSIGN_POC_MODES.CREATE,
      assignPocReadEnum  : APP_SETTINGS.ASSIGN_POC_MODES.READ,
      assignPocUpdateEnum: APP_SETTINGS.ASSIGN_POC_MODES.UPDATE
    };
  }
};
