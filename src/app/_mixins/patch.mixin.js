const MODULE_NAME = 'IdGeneratorMixin';

export const patchMixin = {
  name: MODULE_NAME,
  methods: {
    createPatch(newObj, oldObj) {
      const patch = {};

      for (const key in newObj) {
        if (newObj[key] !== oldObj[key]) {
          patch[key] = newObj[key] ? newObj[key] : null;
        }
      }
      return patch;
    }
  }
};
