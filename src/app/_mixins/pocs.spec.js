import {pocsMixin} from './pocs.mixin';

describe('Pocs Mixin', () => {
  const mixinModule = pocsMixin;

  beforeEach(() => {
  });

  it('should be "PocsMixin"', () => {
    expect(mixinModule.name).toBe('PocsMixin');
  });

  it('getClientFullName should return combined first name and last name from Client object', () => {
    const client = {
      firstName: 'Joe',
      lastName : 'Bloggs'
    };

    const fullName = mixinModule.methods.getClientFullName(client);
    expect(fullName).toBe(client.firstName + ' ' + client.lastName);
  });

  it('getClientFullName should return empty if Client object is null', () => {
    const client = null;

    const fullName = mixinModule.methods.getClientFullName(client);

    expect(fullName).toBe('');
  });

  it('getPocOxygenStatus should return "error"', () => {
    const oxygenPurity = 70;

    const oxygenPurityStatus = mixinModule.methods.getPocOxygenStatus(oxygenPurity);

    expect(oxygenPurityStatus).toBe('error');
  });

  it('getPocOxygenStatus should return "warning"', () => {
    const oxygenPurity = 80;

    const oxygenPurityStatus = mixinModule.methods.getPocOxygenStatus(oxygenPurity);

    expect(oxygenPurityStatus).toBe('warning');
  });

  it('getPocOxygenStatus should return empty', () => {
    const oxygenPurity = 90;

    const oxygenPurityStatus = mixinModule.methods.getPocOxygenStatus(oxygenPurity);

    expect(oxygenPurityStatus).toBe('');
  });

  it('getPocBatteryStatus should return "error"', () => {
    const batteryCycle = 490;

    const batteryCycleStatus = mixinModule.methods.getPocBatteryStatus(batteryCycle);

    expect(batteryCycleStatus).toBe('error');
  });

  it('getPocBatteryStatus should return "warning"', () => {
    const batteryCycle = 460;

    const batteryCycleStatus = mixinModule.methods.getPocBatteryStatus(batteryCycle);

    expect(batteryCycleStatus).toBe('warning');
  });

  it('getPocBatteryStatus should return empty', () => {
    const batteryCycle = 440;

    const batteryCycleStatus = mixinModule.methods.getPocBatteryStatus(batteryCycle);

    expect(batteryCycleStatus).toBe('');
  });
});
