import {loaderMixin} from './loader.mixin';

describe('Loader Mixin', () => {
  const mixinModule = loaderMixin;
  let mixinData = null;

  beforeEach(() => {
    if (typeof mixinModule.data === 'function') {
      mixinData = loaderMixin.data();
    }
  });

  it('should be "LoaderMixin"', () => {
    expect(mixinModule.name).toBe('LoaderMixin');
  });

  it('loaderColor should be "#dad9dd"', () => {
    expect(mixinData.loaderColor).toBe('#dad9dd');
  });
});
