const MODULE_NAME = 'LoaderMixin';

export const loaderMixin = {
  name: MODULE_NAME,
  data() {
    return {
      loaderColor: '#dad9dd'
    };
  }
};
