const MODULE_NAME = 'PocsMixin';

import {APP_SETTINGS} from '../app.settings';

export const pocsMixin = {
  name   : MODULE_NAME,
  methods: {

    /**
     * Returns the full name of the client object.
     * @param client {object}
     * @return {string}
     */
    getClientFullName(client) {
      let clientFullName = '';

      if (client) {
        // register
        if (this.isClientRegistered(client)) {
          clientFullName = client.firstName + ' ' + client.lastName;
        } else {
          clientFullName = client.email;
        }
      }
      return clientFullName;
    },

    /**
     * if first and last naame provided then registered, else assigned only
     * @param client {object}
     * @return {boolean}
     */
    isClientRegistered(client) {
      return Boolean(client.firstName);
    },

    /**
     * Returns class name based on the oxygen purity value.
     * @param oxygenPurity {number}
     * @return {string}
     */
    getPocOxygenStatus(oxygenPurity) {
      let oxygenPurityStatus = '';

      if (oxygenPurity && oxygenPurity < APP_SETTINGS.POC_OXYGEN_PURITY_ERROR_THRESHOLD) {
        oxygenPurityStatus = 'error';
      } else if (oxygenPurity && oxygenPurity < APP_SETTINGS.POC_OXYGEN_PURITY_WARNING_THRESHOLD) {
        oxygenPurityStatus = 'warning';
      }

      return oxygenPurityStatus;
    },

    /**
     * Returns class name based on the battery cycle value.
     * @param batteryCycle {number}
     * @return {string}
     */
    getPocBatteryStatus(batteryCycle) {
      let batteryCycleStatus = '';

      if (batteryCycle > APP_SETTINGS.POC_BATTERY_CYCLES_ERROR_THRESHOLD) {
        batteryCycleStatus = 'error';
      } else if (batteryCycle > APP_SETTINGS.POC_BATTERY_CYCLES_WARNING_THRESHOLD) {
        batteryCycleStatus = 'warning';
      }

      return batteryCycleStatus;
    },

    /**
     * Returns the decimal value of the battery charge value.
     * @param batteryCharge {number}
     * @return {number}
     */
    getPocBatteryChargeDecimal(batteryCharge) {
      return (batteryCharge * 100) / 10000;
    }
  }
};
